import click

from app.models import db
from app.models.user import User

@click.group('user', help='User commands.')
def user():
	pass

@user.command('list', help='Liseds users.')
def listUsers():
	users = User().query.all()

	if 0 < len(users):
		click.echo('ID:\tName:\tE-Mail:')
		for user in users:
			click.echo('%s\t%s\t%s' % (user.id, user.name, user.email))
	else:
		click.echo(click.style('No users found.', fg='red'))

@user.command('add', help='Creates a user.')
@click.option(
	'-n',
	'--name',
	prompt='Username',
	help='Set name of the new user',
	type=str
)
@click.option(
	'-e',
	'--email',
	prompt='E-Mail',
	help='Set E-Mail of the new user',
	type=str
)
@click.option(
	'-p',
	'--password',
	prompt=True,
	hide_input=True,
	confirmation_prompt=True,
	help='Set password of the new user',
	type=str
)
def addUser(name, email, password):
	new_user = User(
		name=name,
		email=email
	)

	new_user.set_password(password)

	try:
		db.session.add(new_user)
		db.session.commit()
		click.echo(
			click.style(
				"Added new user %s." % name,
				fg='green'
			)
		)

	except:
		click.echo(
			click.style(
				"Database Error.",
				fg='red'
			)
		)


@user.command('rm', help='Creates a user.')
@click.option(
	'-id',
	help='User identifier.',
	type=int,
	default=None,
)
@click.option(
	'-n',
	'--name',
	help='Name of the user.',
	type=str,
	default=None,
)
@click.option(
	'-e',
	'--email',
	help='E-Mail of the user.',
	type=str,
	default=None,
)
def removeUser(id, name, email):
	if(not id and not name and not email):
		click.echo(
			click.style(
				"No identifier specified",
				fg='red'
			)
		)
		return

	if id:
		user = User().query.filter_by(id=id).first()
	elif name:
		user = User().query.filter_by(name=name).first()
	elif email:
		user = User().query.filter_by(email=email).first()

	if user:
		try:
			db.session.delete(user)
			db.session.commit()
			click.echo(
				click.style(
					"User removed.",
					fg='green'
				)
			)
		
		except:
			click.echo(
				click.style(
					"Database Error.",
					fg='red'
				)
			)
	else:
		click.echo(
			click.style(
				"No user found.",
				fg='red'
			)
		)
