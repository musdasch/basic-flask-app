from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin
from werkzeug.security import generate_password_hash, check_password_hash


from app.models import db


parser = reqparse.RequestParser()
parser.add_argument(
	'name',
	type=str,
	help='Name of the new user.'
)
parser.add_argument(
	'email',
	type=str,
	help='E-Mail of the new user.'
)
parser.add_argument(
	'password',
	type=str,
	help='Password of the new user.'
)

class User(db.Model, SerializerMixin):
	
	serialize_only = ('id', 'name', 'email')

	id = db.Column(
		db.Integer,
		primary_key=True
	)
	
	name = db.Column(
		db.String(40),
		unique=True,
		nullable=False
	)

	password_hash = db.Column(
		db.String(88)
	)

	email = db.Column(
		db.String(40),
		unique=True
	)

	def set_password(self, password):
		self.password_hash = generate_password_hash(
			password,
			method='sha256'
		)

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __repr__(self):
		return "<User %r>" % (self.name)
