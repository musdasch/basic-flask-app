from flask_restful import Api


from .auth_resource import AuthResource
from .user_resource import UserResource, UsersResource

api = Api()

def init_app(app):
	api.add_resource(AuthResource, '/api/auth/')
	
	api.add_resource(UserResource, '/api/user/<int:id>')
	api.add_resource(UsersResource, '/api/user/')

	api.init_app(app)
