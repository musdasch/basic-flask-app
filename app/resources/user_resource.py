from flask_restful import Resource
from flask_jwt_extended import jwt_required, current_user

from app.models import db, user
from app.models.user import User

class UserResource(Resource):
	@jwt_required()
	def get(self, id):
		_user = User().query.filter_by(id=id).first_or_404()
		return _user.to_dict()

	@jwt_required()
	def put(self, id):
		args = user.parser.parse_args()
		
		_user = User().query.filter_by(id=id).first_or_404()
		_user.name = args.name
		_user.email = args.email
		_user.set_password(args.password)

		try:
			db.session.commit()
			return _user.to_dict(), 200
		
		except:
			return {'error': 'Error updating user'}, 400

	@jwt_required()
	def patch(self, id):
		args = user.parser.parse_args()
		
		_user = User().query.filter_by(id=id).first_or_404()

		if(args.name != None):
			_user.name = args.name

		if(args.email != None):
			_user.email = args.email

		if(args.password != None):
			_user.set_password(args.password)

		try:
			db.session.commit()
			return _user.to_dict(), 200
		
		except:
			return {'error': 'Error updating user'}, 400

	@jwt_required()
	def delete(self, id):
		_user = User().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(_user)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting user'}, 400


class UsersResource(Resource):
	@jwt_required()
	def get(self):
		_users = User().query.all()
		return list(map(lambda user: user.to_dict(), _users))

	@jwt_required()
	def post(self):
		args = user.parser.parse_args()

		_user = User(
			name=args.name,
			email=args.email
		)

		_user.set_password(args.password)

		db.session.add(_user)

		try:
			db.session.commit()
			return _user.to_dict(), 201
		
		except:
			return {'error': 'Error creating user'}, 400
