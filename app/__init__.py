from flask import Flask
from . import auth, commands, models, routes, resources

def create_app():
	app = Flask(__name__)
	app.config.from_object('config.Config')
	
	models.init_app(app)
	auth.init_app(app)
	resources.init_app(app)
	routes.init_app(app)
	commands.init_app(app)
	
	return app


