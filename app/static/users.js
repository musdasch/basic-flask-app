const user_list = document.getElementById('user-list');
const popup = document.getElementById('remove-user-popup');
const popup_username = document.getElementById('remove-user-name');
const remove_user = document.getElementById('remove-user');
const remove_user_cancel = document.getElementById('remove-user-cancel');

if(user_list != null) {
	const tbody = user_list.getElementsByTagName('tbody')[0];

	axios.get('/api/user',{
		headers: {
			'X-CSRF-TOKEN': csrf_access_token
		}
	})
	.then(function (response) {
		let users = response.data;

		users.forEach(user => {
			let row = tbody.insertRow();

			row.id = "user-row-" + user.id;

			let name = row.insertCell();
			let email = row.insertCell();
			let action = row.insertCell();
			
			name.appendChild(
				document.createTextNode(user.name)
			);

			email.appendChild(
				document.createTextNode(user.email)
			);

			let edit_link = document.createElement('a');
			edit_link.href = "/dashboard/users/" + user.id

			edit_link.appendChild(
				document.createTextNode('Edit')
			);

			let remove_link = document.createElement('a');
			remove_link.dataset.id = user.id;
			remove_link.dataset.name = user.username;
			remove_link.classList.add('remove-action');

			remove_link.appendChild(
				document.createTextNode('Remove')
			);
			
			action.appendChild(
				edit_link
			);

			action.appendChild(
				document.createTextNode(', ')
			);

			action.appendChild(
				remove_link
			);

			remove_link.addEventListener('click', (event) => {
				event.preventDefault();

				popup.style.display = "block";

				popup_username.replaceChildren(
					document.createTextNode(
						event.target.dataset.name
					)
				);

				remove_user.dataset.id = event.target.dataset.id;
			});
		});
	})
	.catch(function (error) {
		console.log(error);
	});
}

if(remove_user != null) {
	remove_user.addEventListener('click', (event) => {
		event.preventDefault();

		const user_id = event.target.dataset.id;

		axios.delete('/api/user/' + user_id,
		{
			headers: {
				'X-CSRF-TOKEN': csrf_access_token
			}
		}
		)
		.then((response) => {
			const row = document.getElementById('user-row-' + user_id );
			row.remove();
			popup.style.display = "none";

			popup_username.replaceChildren(
				document.createTextNode(
					''
				)
			);

			remove_user.dataset.id = 0;
		})
		.catch(function (error) {
			console.log(error);
		});
	});
}

if(remove_user_cancel != null) {
	remove_user_cancel.addEventListener('click', (event) => {
		event.preventDefault();

		popup.style.display = "none";

		popup_username.replaceChildren(
			document.createTextNode(
				''
			)
		);

		remove_user.dataset.id = 0;
	});
}