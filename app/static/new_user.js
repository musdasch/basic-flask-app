const access_token = sessionStorage.getItem("access_token");
const new_user_form = document.getElementById('new-user-form');
const info_box = document.getElementById('info');


if(new_user_form != null) {
	new_user_form.addEventListener('submit', (event) => {
		event.preventDefault();

		const form_data = new FormData(new_user_form);

		const name = form_data.get('name');
		const mail = form_data.get('email');
		const pass = form_data.get('password');

		axios.post('/api/user',
		{
			name: name,
			email: mail,
			password: pass
		},
		{
			headers: {
				'X-CSRF-TOKEN': csrf_access_token
			}
		}
		)
		.then(function (response) {
			let user = response.data;
			window.location.href = '/dashboard/users/' + user.id
			info_box.style.display = 'block';
			info_box.appendChild(
				document.createTextNode('Saved...')
			);
		})
		.catch(function (error) {
			console.log(error);
		});
	});
}
