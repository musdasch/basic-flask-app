const access_token = sessionStorage.getItem("access_token");
const edit_user_form = document.getElementById('edit-user-form');
const info_box = document.getElementById('info');

const user_id = document.getElementById('edit-user-form').dataset.user;

if(edit_user_form != null) {
	const id = edit_user_form.dataset.id;

	const name_input = document.getElementById('name');
	const mail_input = document.getElementById('email');
	const pass_input = document.getElementById('password');

	axios.get('/api/user/' + id,{
		headers: {
			'X-CSRF-TOKEN': csrf_access_token
		}
	})
	.then(function (response) {
		let user = response.data;

		name_input.value = user.name;
		mail_input.value = user.email;
		pass_input.value = '';
	})
	.catch(function (error) {
		info_box.style.display = 'block';
		info_box.appendChild(
			document.createTextNode(error)
		);
	});


	edit_user_form.addEventListener('submit', (event) => {
		event.preventDefault();

		const form_data = new FormData(edit_user_form);

		const name = form_data.get('name');
		const mail = form_data.get('email');
		const pass = form_data.get('password');

		if(pass.length === 0) {
			axios.patch('/api/user/' + id,
			{
				username: name,
				email: mail,
			},
			{
				headers: {
					'X-CSRF-TOKEN': csrf_access_token
				}
			}
			)
			.then(function (response) {
				let user = response.data;

				name_input.value = user.name;
				mail_input.value = user.email;
				pass_input.value = '';

				info_box.style.display = 'block';

				info_box.appendChild(
					document.createTextNode('Saved...')
				);
			})
			.catch(function (error) {
				console.log(error);
			});
		} else {
			axios.put('/api/user/' + id,
			{
				name: name,
				email: mail,
				password: pass
			},
			{
				headers: {
					'X-CSRF-TOKEN': csrf_access_token
				}
			}
			)
			.then(function (response) {
				let user = response.data;

				name_input.value = user.name;
				mail_input.value = user.email;
				pass_input.value = '';

				info_box.style.display = 'block';

				info_box.appendChild(
					document.createTextNode('Saved...')
				);
			})
			.catch(function (error) {
				console.log(error);
			});
		}
	});
}
