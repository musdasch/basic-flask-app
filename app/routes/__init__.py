from . import index, auth, control

def init_app(app):
    app.register_blueprint(index.bp)
    app.register_blueprint(auth.bp)
    app.register_blueprint(control.bp)

