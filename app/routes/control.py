from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_jwt_extended import jwt_required, current_user

from app.models import db
from app.models.user import User


bp = Blueprint('control', __name__, url_prefix='/dashboard')

@bp.route("/", methods=['GET','POST'])
@jwt_required()
def dashboard():
	return render_template("dashboard.html")

## ***************Users********************

@bp.route("/users", methods=['GET'])
@jwt_required()
def users():
	return render_template("users.html")

@bp.route("/users/<int:user_id>", methods=['GET','POST'])
@jwt_required()
def edit_user(user_id):
	return render_template("edit_user.html", id=user_id, user_id=current_user.id)

@bp.route("/new_user", methods=['GET'])
@jwt_required()
def new_user():
	return render_template("new_user.html")
