from flask import Blueprint, render_template, flash, redirect, url_for,request
from flask_jwt_extended import create_access_token, set_access_cookies, unset_jwt_cookies, current_user, jwt_required

from app.forms.login import LoginForm
from app.models.user import User


bp = Blueprint('auth', __name__)

@bp.route("/login", methods=['GET', 'POST'])
@jwt_required(optional=True)
def login():
	form = LoginForm()
	
	if current_user:
		return redirect(url_for('control.dashboard'))

	if request.method=="POST":
		user = User().query.filter_by(name=form.username.data).first()
		if user is not None: 
			if user.check_password(form.password.data):
				response = redirect(url_for('control.dashboard'))
				access_token = create_access_token(identity=user)
				set_access_cookies(response,access_token)
				return response
			else:
				flash("Invalid Username or password!", "danger")
		else:
			flash("Invalid Username or password!", "danger")

	return render_template("auth.html", form=form)
	

@bp.route("/logout", methods=['GET', 'POST'])
def logout():
	response = redirect(url_for('auth.login'))
	unset_jwt_cookies(response)
	return response
