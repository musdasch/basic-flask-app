from flask import *
from flask_jwt_extended import jwt_required

from app.models.user import User

bp = Blueprint('index', __name__)

@bp.route("/", methods=['GET', 'POST'])
@jwt_required(optional=True)
def index():
    return render_template("welcome.html")