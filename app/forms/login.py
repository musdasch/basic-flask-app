from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, Length, Email, Optional

class LoginForm(FlaskForm):
	username = StringField(validators=[InputRequired(), Length(1, 64)])
	password = PasswordField(validators=[InputRequired(), Length(min=8, max=72)])