# Basic Flask APP
A basic flask Application.

## Setup
```
#Debian only
sudo apt install default-libmysqlclient-dev

#Fedora
sudo dnf install python python-devel mysql-devel redhat-rpm-config gcc

cd [project_folder]
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

## Start
```
# In oder to start de vitual emviremend.
source venv/bin/activate

# In oder to start flask
flask --debug run
```

## Initializ the database
```
flask db init
flask db migrate -m "Initial migration."
flask db upgrade
```

## Update the database
```
flask db migrate -m "Changed User attribute username to name."
flask db upgrade
```

## User CLI
```

#User
flask user --help 													#Help page
flask user add [-n|--name <name>] [-e|--email <email>]				#New user
flask user rm [-id <id>] [-n|--name <name>] [-e|--email <email>]	#Remove user
flask user list 													#List users

```


## License
Copyright © 2022 Musdasch <musdasch@protonmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.